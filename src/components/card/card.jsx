import React, { Component } from "react";


class Card extends Component {

    render() {
        return (
            <div>
                <h3>Namaku {this.props.nama}</h3>
                <h4>{this.props.alamat} tempat tinggalku</h4>
            </div>
        )
    }
}
export default Card