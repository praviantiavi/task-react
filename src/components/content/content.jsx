import React from "react"
import Card from "../card/card"

class Content extends React.Component {

        state = {
            nama: "Avi",
            alamat: "Malang"
        }

        

    render() {
        return (
            <div>
                <Card nama={this.state.nama} alamat={this.state.alamat}/>
                <button onClick={() => this.setState({nama:"Pravianti"})}>Ubah nama</button>
                <button onClick={() => this.setState({alamat:"Jakarta"})}>Ubah alamat</button>
            </div>
        )
    }
        
}

export default Content